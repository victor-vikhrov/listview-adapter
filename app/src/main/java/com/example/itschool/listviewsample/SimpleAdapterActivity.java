package com.example.itschool.listviewsample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class SimpleAdapterActivity extends AppCompatActivity {

    ListView listView;
    SimpleAdapter adapter;

    final String KEY_IMAGE = "image";
    final String KEY_TITLE = "title";
    final String KEY_DESCRIPTION = "description";

    final String[] from = {KEY_IMAGE, KEY_TITLE, KEY_DESCRIPTION};
    final int[] to = {R.id.iv_avatar, R.id.tv_title, R.id.tv_description};
    ArrayList<HashMap<String, Object>> data = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_adapter);
        listView = (ListView) findViewById(R.id.lv_list);

        for (int i = 0; i < 20; i++) {
            HashMap<String, Object> hm = new HashMap<>();
            hm.put(KEY_IMAGE, R.drawable.pic);
            hm.put(KEY_TITLE, "Theme 4: Java Collections");
            hm.put(KEY_DESCRIPTION, "Chapter 2: ArrayList and LinkedList different...");
            data.add(hm);
        }
        data.get(5).put(KEY_TITLE, "Theme 5: WWW");

        adapter = new SimpleAdapter(this, data, R.layout.list_item, from, to);

        listView.setAdapter(adapter);
    }
}
