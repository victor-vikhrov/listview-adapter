package com.example.itschool.listviewsample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ArrayAdapterActivity extends AppCompatActivity {

    ListView listView;
    ArrayAdapter<String> adapter;

    ArrayList<String> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_array_adapter);

        listView = (ListView) findViewById(R.id.lv_list);

        data.add(new String("Linux OS"));
        data.add(new String("Android OS"));
        data.add(new String("Mac OS"));

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_2, data);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(ArrayAdapterActivity.this, data.get(i), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
