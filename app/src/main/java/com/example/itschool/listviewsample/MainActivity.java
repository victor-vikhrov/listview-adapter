package com.example.itschool.listviewsample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.b_array_adapter).setOnClickListener(this);
        findViewById(R.id.b_simple_adapter)
                .setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.b_array_adapter:
                startActivity(new Intent(this, ArrayAdapterActivity.class));
                break;
            case R.id.b_simple_adapter:
                startActivity(new Intent(this, SimpleAdapterActivity.class));
                break;
        }
    }
}
